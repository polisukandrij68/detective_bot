import { createBot } from './bot/createBot';
import { config } from 'dotenv';

config();

const bootstrap = async () => {
  const bot = await createBot();

  console.log('Bot launched');
  await bot.launch();
};

bootstrap();
