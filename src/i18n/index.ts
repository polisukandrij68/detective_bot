import { I18n } from 'i18n';
import path from 'path';

// t => means translation (this is tradition from i18next library)
export const createI18n = () =>
  new I18n({
    locales: ['en', 'uk'],
    directory: path.resolve(__dirname, '../../locales'),
    defaultLocale: 'en',
    objectNotation: true,
  });
