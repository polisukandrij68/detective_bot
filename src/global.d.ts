import { User } from '@prisma/client';
import { I18n } from 'i18n';
import { PromptOptions } from './bot/middlewares/promptMiddleware/PromptStack';
import { BotMenuList } from './bot/middlewares/menuMiddleware/BotMenuList';

declare module 'telegraf' {
  interface Context {
    t: I18n;
    prompt: (question: string, options?: PromptOptions) => Promise<string>;
    user: User;
    menu: BotMenuList;
  }
}
