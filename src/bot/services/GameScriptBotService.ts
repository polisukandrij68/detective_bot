import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { prisma } from '../../prisma';

export class GameScriptBotService {
  static async getGameScript(ctx: Context<Update> & { match: RegExpExecArray }) {
    const scriptId = +ctx.match[1];

    if (Number.isNaN(scriptId)) {
      throw new Error('script Id is not valid');
    }

    const script = await prisma.gameScript.findUnique({
      where: {
        id: scriptId,
      },
    });

    if (!script) {
      throw new Error('script with such id not found');
    }

    return script;
  }
}
