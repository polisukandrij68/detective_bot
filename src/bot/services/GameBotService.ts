import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { prisma } from '../../prisma';

export class GameBotService {
  static async getGame(ctx: Context<Update> & { match: RegExpExecArray }) {
    const gameId = +ctx.match[1];

    if (Number.isNaN(gameId)) {
      throw new Error('script Id is not valid');
    }

    const game = await prisma.game.findUnique({
      where: {
        id: gameId,
      },
    });

    if (!game) {
      throw new Error('game with such id not found');
    }

    return game;
  }

  static async getHasAssignedRoleOrGame(userId: number) {
    const hasAssignedRole = await this.getHasAssignedRole(userId);
    const hasAssignedGame = await this.getHasAssignedGame(userId);

    return hasAssignedRole || hasAssignedGame;
  }

  static async getHasAssignedRole(userId: number) {
    const roleAssignment = await prisma.playerRoleAssignment.findFirst({
      where: {
        userId,
      },
    });

    return Boolean(roleAssignment);
  }

  static async getHasAssignedGame(userId: number) {
    const game = await prisma.game.findFirst({
      where: {
        adminId: userId,
      },
    });

    return Boolean(game);
  }
}
