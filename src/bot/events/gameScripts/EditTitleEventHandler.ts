import { BaseEventHandler } from '../BaseEventHandler';
import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { GameScriptBotService } from '../../services/GameScriptBotService';
import { prisma } from '../../../prisma';
import { MainMenuEventHandler } from '../MainMenuEventHandler';

export class EditTitleEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/gameScript.editTitle:(.+)/, async (ctx) => {
      try {
        const script = await GameScriptBotService.getGameScript(ctx);

        const newTitle = await ctx.prompt(ctx.t.__('openGameScript.editTitleMenu.prompt'), { clearAfterAnswer: true });

        await prisma.gameScript.update({
          where: {
            id: script.id,
          },
          data: {
            name: newTitle,
          },
        });

        ctx.reply('openGameScript.editTitleMenu.success');
        await MainMenuEventHandler.openMainMenu(ctx);
      } catch {
        await this.replyError(ctx);
      }
    });
  }
  private async replyError(ctx: Context<Update>) {
    await ctx.reply(ctx.t.__('openGameScript.fail'));
  }
}
