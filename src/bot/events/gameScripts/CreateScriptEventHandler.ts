import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { GameScriptButtons } from '../../markup/GameScriptButtons';

export class CreateScriptEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('gameScript.create', async (ctx) => {
      try {
        await ctx.menu.replyAsMenuMessage(ctx.t.__('createScript.title'));

        const name = await ctx.prompt(ctx.t.__('createScript.namePrompt'), { clearAfterAnswer: true });
        const prologue = await ctx.prompt(ctx.t.__('createScript.prologuePrompt'), {
          clearAfterAnswer: true,
          optional: true,
        });
        const epilogue = await ctx.prompt(ctx.t.__('createScript.epiloguePrompt'), {
          clearAfterAnswer: true,
          optional: true,
        });

        const script = await prisma.gameScript.create({
          data: {
            name,
            prologue,
            epilogue,
            authorId: ctx.user.id,
          },
        });

        await ctx.menu.replyAsMenuMessage(ctx.t.__('createScript.success'), GameScriptButtons(script, ctx.t));
      } catch (e) {
        console.error(e);
        ctx.reply(ctx.t.__('createScript.fail'));
      }
    });
  }
}
