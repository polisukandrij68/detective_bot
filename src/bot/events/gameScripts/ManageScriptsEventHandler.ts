import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { ManageScriptsButtons } from '../../markup/ManageScriptsButtons';

export class ManageScriptsEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('gameScript.manageScripts', async (ctx) => {
      const authorId = ctx.user.id;

      const scripts = await prisma.gameScript.findMany({
        where: {
          authorId,
        },
      });

      if (!scripts.length) {
        await ctx.menu.replyAsMenuMessage(ctx.t.__('manageScripts.noRecords'), ManageScriptsButtons(scripts, ctx.t));

        return;
      }

      await ctx.menu.replyAsMenuMessage(ctx.t.__('manageScripts.title'), ManageScriptsButtons(scripts, ctx.t));
    });
  }
}
