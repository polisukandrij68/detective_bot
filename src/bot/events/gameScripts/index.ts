import { CreateScriptEventHandler } from './CreateScriptEventHandler';
import { EditTitleEventHandler } from './EditTitleEventHandler';
import { ManageScriptsEventHandler } from './ManageScriptsEventHandler';
import { OpenGameScriptEventHandler } from './OpenGameScriptEventHandler';

export const gameScriptsEvents = [
  CreateScriptEventHandler,
  EditTitleEventHandler,
  ManageScriptsEventHandler,
  OpenGameScriptEventHandler,
];
