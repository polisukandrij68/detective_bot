import { BaseEventHandler } from '../BaseEventHandler';
import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { SingleScriptManageButtons } from '../../markup/SingleScriptManageButtons';
import { GameScriptBotService } from '../../services/GameScriptBotService';

export class OpenGameScriptEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/gameScript.open:(.+)/, async (ctx) => {
      try {
        const script = await GameScriptBotService.getGameScript(ctx);

        await ctx.menu.replyAsMenuMessage(
          ctx.t.__mf('openGameScript.title', { name: script.name }),
          SingleScriptManageButtons(script, ctx.t)
        );
      } catch {
        await this.replyError(ctx);
      }
    });
  }

  private async replyError(ctx: Context<Update>) {
    await ctx.reply(ctx.t.__('openGameScript.fail'));
  }
}
