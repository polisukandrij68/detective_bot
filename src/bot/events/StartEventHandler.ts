import { BaseEventHandler } from './BaseEventHandler';
import { MainMenuButtons } from '../markup/MainMenuButtons';

export class StartEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.start(async (ctx) => {
      await ctx.menu.replyAsMenuMessage(ctx.t.__('start'), MainMenuButtons(ctx.t));
    });
  }
}
