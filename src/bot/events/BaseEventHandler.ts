import { Telegraf } from 'telegraf';

export abstract class BaseEventHandler {
  constructor(protected readonly bot: Telegraf) {}

  abstract subscribe(): Promise<void>;
}
