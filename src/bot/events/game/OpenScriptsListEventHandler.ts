import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { ScriptsListButtons } from '../../markup/ScriptsListButtons';
import { OpenManageScriptsMenuButton } from '../../markup/OpenManageScriptsMenuButton';

export class OpenScriptsListEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('scriptsList.open', async (ctx) => {
      const scripts = await prisma.gameScript.findMany();

      if (!scripts.length) {
        await ctx.menu.replyAsMenuMessage(ctx.t.__('scriptsList.noRecords'), OpenManageScriptsMenuButton(ctx.t));

        return;
      }

      await ctx.menu.replyAsMenuMessage(ctx.t.__('scriptsList.title'), ScriptsListButtons(scripts, ctx.t));
    });
  }
}
