import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { MainMenuEventHandler } from '../MainMenuEventHandler';

export class GameLeaveEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('game.leave', async (ctx) => {
      try {
        await prisma.playerRoleAssignment.delete({
          where: {
            userId: ctx.user.id,
          },
        });

        await ctx.reply(ctx.t.__('gameLeave.message'));

        await MainMenuEventHandler.openMainMenu(ctx);
      } catch {
        await ctx.reply(ctx.t.__('gameLeave.fail'));
      }
    });
  }
}
