import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { GamesList } from '../../markup/GamesList';
import { GameBotService } from '../../services/GameBotService';

export class GamesListEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('game.list', async (ctx) => {
      try {
        const hasAssignment = await GameBotService.getHasAssignedRoleOrGame(ctx.user.id);

        if (hasAssignment) {
          ctx.reply(ctx.t.__('gamesList.hasHostedGame'));

          return;
        }

        const games = await prisma.game.findMany({
          select: {
            id: true,
            GameScript: true,
            Admin: true,
          },
        });

        if (!games.length) {
          ctx.reply(ctx.t.__('gamesList.noRecords'));

          return;
        }

        await ctx.menu.replyAsMenuMessage(ctx.t.__('gamesList.title'), GamesList(games, ctx.t));
      } catch {
        ctx.reply(ctx.t.__('gamesList.fail'));
      }
    });
  }
}
