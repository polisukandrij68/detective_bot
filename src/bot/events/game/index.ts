import { OpenScriptsListEventHandler } from './OpenScriptsListEventHandler';
import { StartGameEventHandler } from './StartGameEventHandler';
import { StopGameEventHandler } from './StopGameEventHandler';
import { GamesListEventHandler } from './GamesListEventHandler';
import { JoinGameEvenHandler } from './JoinGameEvenHandler';
import { GameLeaveEventHandler } from './GameLeaveEventHandler';

export const gameEvents = [
  OpenScriptsListEventHandler,
  StartGameEventHandler,
  StopGameEventHandler,
  GamesListEventHandler,
  JoinGameEvenHandler,
  GameLeaveEventHandler,
];
