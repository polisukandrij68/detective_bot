import { BaseEventHandler } from '../BaseEventHandler';
import { GameScriptBotService } from '../../services/GameScriptBotService';
import { prisma } from '../../../prisma';
import { StopGameButton } from '../../markup/StopGameButton';

export class StartGameEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/game.start:(.+)/, async (ctx) => {
      try {
        const script = await GameScriptBotService.getGameScript(ctx);
        const startedGame = await this.getStartedGame(ctx.user.id);

        if (startedGame) {
          await ctx.menu.replyAsMenuMessage(
            ctx.t.__('gameStart.gameAlreadyStarted'),
            StopGameButton(startedGame.id, ctx.t)
          );

          return;
        }

        const game = await prisma.game.create({
          data: {
            adminId: ctx.user.id,
            gameScriptId: script.id,
          },
        });

        await ctx.menu.replyAsMenuMessage(ctx.t.__('gameStart.success'), StopGameButton(game.id, ctx.t));
      } catch (e) {
        console.log(e);

        ctx.reply(ctx.t.__('gameStart.fail'));
      }
    });
  }

  private async getStartedGame(userId: number) {
    const game = await prisma.game.findFirst({
      where: {
        adminId: userId,
      },
    });

    return game;
  }
}
