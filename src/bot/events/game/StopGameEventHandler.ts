import { BaseEventHandler } from '../BaseEventHandler';
import { GameBotService } from '../../services/GameBotService';
import { prisma } from '../../../prisma';
import { MainMenuButtons } from '../../markup/MainMenuButtons';
import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';

export class StopGameEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/game.stop:(.+)/, async (ctx) => {
      try {
        const game = await GameBotService.getGame(ctx);
        const gameScript = await prisma.gameScript.findUnique({
          where: {
            id: game.gameScriptId,
          },
        });

        const rolesAssignments = await prisma.playerRoleAssignment.findMany({
          where: {
            gameId: game.id,
          },
        });

        await prisma.playerRoleAssignment.deleteMany({
          where: {
            gameId: game.id,
          },
        });

        const playersIds = rolesAssignments.map((rolesAssignment) => rolesAssignment.userId);
        await this.notifyGameEnd({
          usersIds: playersIds,
          ctx,
          epilogue: gameScript?.epilogue || undefined,
        });

        await prisma.game.delete({
          where: {
            id: game.id,
          },
        });

        await ctx.menu.replyAsMenuMessage(ctx.t.__('gameStop.success'), MainMenuButtons(ctx.t));
      } catch {
        ctx.reply(ctx.t.__('gameStop.fail'));
      }
    });
  }

  private async notifyGameEnd(params: { usersIds: number[]; ctx: Context<Update>; epilogue?: string }) {
    const { usersIds, ctx, epilogue } = params;

    const users = await prisma.user.findMany({
      where: {
        id: {
          in: usersIds,
        },
      },
    });

    for (const user of users) {
      const sendMessage = async (message: string) => {
        await ctx.telegram.sendMessage(user.telegramId.toString(), message);
      };

      if (epilogue) {
        await sendMessage(epilogue);
      }

      await sendMessage(ctx.t.__('gameStop.stopNotification'));
    }
  }
}
