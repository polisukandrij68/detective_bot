import { BaseEventHandler } from '../BaseEventHandler';
import { GameBotService } from '../../services/GameBotService';
import { prisma } from '../../../prisma';
import { Game, Role } from '@prisma/client';
import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { LeaveGameButton } from '../../markup/LeaveGameButton';

export class JoinGameEvenHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/game.join:(.+)/, async (ctx) => {
      try {
        const hasAssignment = await GameBotService.getHasAssignedRoleOrGame(ctx.user.id);

        if (hasAssignment) {
          ctx.reply(ctx.t.__('joinGame.hasAssignment'));

          return;
        }

        const game = await GameBotService.getGame(ctx);
        const role = await this.getAvailableRole(game);
        const prologue = await this.getGamePrologue(game.gameScriptId);

        if (!role) {
          await ctx.reply(ctx.t.__('joinGame.noAvailableRole'));

          return;
        }

        await ctx.menu.replyAsMenuMessage(ctx.t.__mf('joinGame.roleText', role), LeaveGameButton(ctx.t));

        if (prologue) {
          await ctx.menu.replyAndAppendToCurrentMenu(prologue);
        }

        const roleAssignment = await this.assignRole(game.id, role.id, ctx.user.id);

        await this.notifyAdminAboutRoleAssignment(ctx, roleAssignment.id);
      } catch {
        ctx.reply(ctx.t.__('joinGame.fail'));
      }
    });
  }

  private async getAvailableRole(game: Game): Promise<Role | undefined> {
    const assignedRoles = await prisma.playerRoleAssignment.findMany({
      where: {
        gameId: game.id,
      },
    });
    const allRoles = await prisma.role.findMany({
      where: {
        gameScriptId: game.gameScriptId,
      },
    });

    const assignedRolesIds = assignedRoles.map((roleAssignment) => roleAssignment.roleId);

    const availableRoles = allRoles.filter((role) => !assignedRolesIds.includes(role.id));

    return availableRoles[0];
  }

  private async getGamePrologue(gameScriptId: number) {
    const gameScript = await prisma.gameScript.findUnique({
      where: {
        id: gameScriptId,
      },
    });

    return gameScript?.prologue;
  }

  private async assignRole(gameId: number, roleId: number, userId: number) {
    return await prisma.playerRoleAssignment.create({
      data: {
        gameId: gameId,
        roleId: roleId,
        userId: userId,
      },
    });
  }

  private async notifyAdminAboutRoleAssignment(ctx: Context<Update>, roleAssignmentId: number) {
    const roleAssignment = await prisma.playerRoleAssignment.findUnique({
      where: {
        id: roleAssignmentId,
      },
      select: {
        Game: {
          select: {
            Admin: true,
          },
        },
        User: true,
        Role: true,
      },
    });

    if (!roleAssignment) {
      return;
    }

    await ctx.telegram.sendMessage(
      roleAssignment.Game.Admin.telegramId.toString(),
      ctx.t.__mf('joinGame.adminNotification', {
        userName: roleAssignment.User.name,
        roleName: roleAssignment.Role.name,
      })
    );
  }
}
