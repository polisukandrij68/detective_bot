import { BaseEventHandler } from './BaseEventHandler';
import { MainMenuButtons } from '../markup/MainMenuButtons';
import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';

export class MainMenuEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action('mainMenu.open', (ctx) => {
      MainMenuEventHandler.openMainMenu(ctx);
    });
  }

  static async openMainMenu(ctx: Context<Update>) {
    await ctx.menu.replyAsMenuMessage(ctx.t.__('mainMenu.title'), MainMenuButtons(ctx.t));
  }
}
