import { StartEventHandler } from './StartEventHandler';
import { Telegraf } from 'telegraf';
import { MainMenuEventHandler } from './MainMenuEventHandler';
import { gameScriptsEvents } from './gameScripts';
import { rolesEvents } from './roles';
import { gameEvents } from './game';

export class BotActionFabric {
  private static commands = [
    StartEventHandler,
    MainMenuEventHandler,
    ...gameScriptsEvents,
    ...rolesEvents,
    ...gameEvents,
  ];

  static async attach(bot: Telegraf) {
    for (const Command of this.commands) {
      await new Command(bot).subscribe();
    }
  }
}
