import { AddRoleEventHandler } from './AddRoleEventHandler';
import { EditRoleEventHandler } from './EditRoleEventHandler';

export const rolesEvents = [AddRoleEventHandler, EditRoleEventHandler];
