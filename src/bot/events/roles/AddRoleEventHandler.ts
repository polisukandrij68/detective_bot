import { BaseEventHandler } from '../BaseEventHandler';
import { prisma } from '../../../prisma';
import { GameScriptButtons } from '../../markup/GameScriptButtons';
import { numericValidate } from '../../middlewares/promptMiddleware/validators';

export class AddRoleEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/gameScript\.addRole:(.+)/, async (ctx) => {
      try {
        const scriptId = +ctx.match[1];

        if (Number.isNaN(scriptId)) {
          ctx.reply(ctx.t.__('addRole.fail'));

          return;
        }

        const script = await prisma.gameScript.findUnique({ where: { id: scriptId } });

        if (!script) {
          ctx.reply(ctx.t.__('addRole.fail'));

          return;
        }

        await ctx.menu.closeCurrentMenu();
        const promptOptions = { clearAfterAnswer: true };

        const name = await ctx.prompt(ctx.t.__('addRole.namePrompt'), promptOptions);
        const age = await ctx.prompt(ctx.t.__('addRole.agePrompt'), {
          ...promptOptions,
          validate: numericValidate(ctx.t),
        });
        const story = await ctx.prompt(ctx.t.__('addRole.storyPrompt'), promptOptions);

        await this.createRole({
          gameScriptId: scriptId,
          name,
          age,
          story,
        });

        await ctx.menu.replyAsMenuMessage(ctx.t.__('addRole.success'), GameScriptButtons(script, ctx.t));
      } catch (e) {
        console.error(e);

        ctx.reply(ctx.t.__('addRole.fail'));
      }
    });
  }

  private async createRole(data: { gameScriptId: number; name: string; age: string; story: string }) {
    return prisma.role.create({
      data,
    });
  }
}
