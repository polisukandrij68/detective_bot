import { BaseEventHandler } from '../BaseEventHandler';
import { GameScriptBotService } from '../../services/GameScriptBotService';
import { prisma } from '../../../prisma';
import { RolesListButtons } from '../../markup/RolesListButtons';

export class EditRoleEventHandler extends BaseEventHandler {
  async subscribe(): Promise<void> {
    this.bot.action(/gameScript.editRoles:(.+)/, async (ctx) => {
      try {
        const script = await GameScriptBotService.getGameScript(ctx);
        const roles = await prisma.role.findMany({
          where: {
            gameScriptId: script.id,
          },
        });

        if (!roles.length) {
          const message = await ctx.reply(ctx.t.__('editRoles.noRecords'));
          ctx.menu.markMessageAsMenu(message.message_id);

          return;
        }

        await ctx.menu.replyAsMenuMessage(
          ctx.t.__mf('editRoles.title', { scriptName: script.name }),
          RolesListButtons(roles, ctx.t)
        );
      } catch {
        ctx.reply(ctx.t.__('editRoles.fail'));
      }
    });
  }
}
