import { GameScript } from '@prisma/client';
import { I18n } from 'i18n';
import { Markup } from 'telegraf';
import { MainMenuButton } from './MainMenuButton';

export const GameScriptButtons = (gameScript: GameScript, t: I18n) => {
  return Markup.inlineKeyboard([
    [Markup.button.callback(t.__('gameScript.addRole'), `gameScript.addRole:${gameScript.id}`)],
    [
      MainMenuButton(t),
      Markup.button.callback(t.__('gameScript.manageScripts'), 'gameScript.manageScripts'),
    ],
  ]);
};
