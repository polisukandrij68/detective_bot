import { I18n } from 'i18n';
import { GameScript, User } from '@prisma/client';
import { Markup } from 'telegraf';
import { MainMenuButton } from './MainMenuButton';

interface ListItem {
  id: number;
  GameScript: GameScript;
  Admin: User;
}

export const GamesList = (games: ListItem[], t: I18n) => {
  return Markup.inlineKeyboard([
    ...games.map((game) => [
      Markup.button.callback(`${game.Admin.name}: ${game.GameScript.name}`, `game.join:${game.id}`),
    ]),
    [MainMenuButton(t)],
  ]);
};
