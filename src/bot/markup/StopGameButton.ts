import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const StopGameButton = (gameId: number, t: I18n) => {
  return Markup.inlineKeyboard([Markup.button.callback(t.__('gameStop.stop'), `game.stop:${gameId}`)]);
};
