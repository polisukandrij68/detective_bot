import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const SkipPromptButton = (t: I18n) => {
  return Markup.inlineKeyboard([Markup.button.callback(t.__('prompt.skip'), 'prompt.skip')]);
};
