import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const MainMenuButton = (t: I18n) => {
  return Markup.button.callback(t.__('mainMenu.title'), 'mainMenu.open');
};
