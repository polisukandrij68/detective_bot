import { GameScript } from '@prisma/client';
import { I18n } from 'i18n';
import { Markup } from 'telegraf';
import { MainMenuButton } from './MainMenuButton';

export const ScriptsListButtons = (scripts: GameScript[], t: I18n) => {
  return Markup.inlineKeyboard([
    ...scripts.map((script) => [Markup.button.callback(script.name, `game.start:${script.id}`)]),
    [MainMenuButton(t)],
  ]);
};
