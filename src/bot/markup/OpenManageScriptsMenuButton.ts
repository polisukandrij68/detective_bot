import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const OpenManageScriptsMenuButton = (t: I18n) => {
  return Markup.inlineKeyboard([Markup.button.callback(t.__('gameScript.manageScripts'), 'gameScript.manageScripts')]);
};
