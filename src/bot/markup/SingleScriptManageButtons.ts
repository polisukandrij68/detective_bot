import { GameScript } from '@prisma/client';
import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const SingleScriptManageButtons = (script: GameScript, t: I18n) => {
  return Markup.inlineKeyboard([
    [Markup.button.callback(t.__('openGameScript.editTitle'), `gameScript.editTitle:${script.id}`)],
    [Markup.button.callback(t.__('openGameScript.addRole'), `gameScript.addRole:${script.id}`)],
    [Markup.button.callback(t.__('openGameScript.editRoles'), `gameScript.editRoles:${script.id}`)],
    [Markup.button.callback(t.__('gameScript.manageScripts'), 'gameScript.manageScripts')],
  ]);
};
