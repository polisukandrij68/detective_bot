import { GameScript } from '@prisma/client';
import { I18n } from 'i18n';
import { Markup } from 'telegraf';
import { MainMenuButton } from './MainMenuButton';

export const ManageScriptsButtons = (scripts: GameScript[], t: I18n) => {
  const buttons = [
    ...scripts.map((script) => [Markup.button.callback(script.name, `gameScript.open:${script.id}`)]),
    [
      MainMenuButton(t),
      Markup.button.callback(t.__('gameScript.create'), 'gameScript.create'),
    ],
  ];

  return Markup.inlineKeyboard(buttons);
};
