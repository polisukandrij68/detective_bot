import { Markup } from 'telegraf';
import { I18n } from 'i18n';

export const MainMenuButtons = (t: I18n) => {
  return Markup.inlineKeyboard([
    [Markup.button.callback(t.__('mainMenu.joinGame'), 'game.list')],
    [
      Markup.button.callback(t.__('mainMenu.startGame'), 'scriptsList.open'),
      Markup.button.callback(t.__('gameScript.manageScripts'), 'gameScript.manageScripts'),
    ],
  ]);
};
