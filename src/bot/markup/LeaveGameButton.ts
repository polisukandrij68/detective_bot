import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const LeaveGameButton = (t: I18n) => {
  return Markup.inlineKeyboard([Markup.button.callback(t.__('gameLeave.title'), 'game.leave')]);
};
