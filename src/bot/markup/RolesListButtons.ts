import { Role } from '@prisma/client';
import { I18n } from 'i18n';
import { Markup } from 'telegraf';

export const RolesListButtons = (roles: Role[], t: I18n) => {
  const [{ gameScriptId }] = roles;

  const rolesButtons = roles.map((role) => [Markup.button.callback(role.name, `role.open:${role.id}`)]);

  return Markup.inlineKeyboard([
    ...rolesButtons,
    [
      Markup.button.callback(t.__('editRoles.backToScript'), `gameScript.open:${gameScriptId}`),
      Markup.button.callback(t.__('gameScript.manageScripts'), 'gameScript.manageScripts'),
    ],
  ]);
};
