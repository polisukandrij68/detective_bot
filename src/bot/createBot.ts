import { Telegraf } from 'telegraf';
import { userMiddleware } from './middlewares/userMiddleware';
import { i18nMiddleware } from './middlewares/i18nMiddleware';
import { BotActionFabric } from './events/BotActionsFabric';
import { BotMenuList } from './middlewares/menuMiddleware/BotMenuList';
import { menuMiddleware } from './middlewares/menuMiddleware/menuMiddleware';
import { PromptMiddleware } from './middlewares/promptMiddleware/PromptMiddleware';

export const createBot = async () => {
  const botToken = process.env.BOT_TOKEN;

  if (!botToken) {
    console.log('BOT_TOKEN is not defined in the .env file');

    process.exit(1);
  }

  const bot = new Telegraf(botToken);

  const botMenuList = new BotMenuList();
  const promptMiddleware = new PromptMiddleware(bot);

  bot.use(userMiddleware, i18nMiddleware, promptMiddleware.handler, menuMiddleware(botMenuList));
  await BotActionFabric.attach(bot);

  return bot;
};
