import { Context } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';

export class BotMenuList {
  private messagesIds: Partial<Record<number, number[]>> = {};
  private _ctx: Context<Update> | undefined;
  private _userId: number | undefined;

  private get ctx() {
    if (!this._ctx) {
      throw new Error('Context is not defined');
    }

    return this._ctx;
  }
  private get userId() {
    if (!this._userId) {
      throw new Error('userId is not defined');
    }

    return this._userId;
  }

  constructor() {}

  setContext(ctx: Context<Update>) {
    this._ctx = ctx;
  }
  setUser(userId: number) {
    this._userId = userId;
  }

  public markMessageAsMenu(messageId: number) {
    if (!this.messagesIds[this.userId]) {
      this.messagesIds[this.userId] = [messageId];
    } else {
      this.messagesIds[this.userId]?.push?.(messageId);
    }
  }

  public async closeCurrentMenu() {
    const messagesIds = this.messagesIds[this.userId] || [];
    this.messagesIds[this.userId] = [];

    for (const messageId of messagesIds) {
      await this.ctx.deleteMessage(messageId);
    }
  }

  async replyAsMenuMessage(...params: Parameters<Context<Update>['reply']>) {
    await this.closeCurrentMenu();

    return this.replyAndAppendToCurrentMenu(...params);
  }

  async replyAndAppendToCurrentMenu(...params: Parameters<Context<Update>['reply']>) {
    const message = await this.ctx.reply(...params);

    this.markMessageAsMenu(message.message_id);

    return message;
  }
}
