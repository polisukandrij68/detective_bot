import { Context, Middleware } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { BotMenuList } from './BotMenuList';

export const menuMiddleware =
  (botMenu: BotMenuList): Middleware<Context<Update>> =>
  async (ctx, next) => {
    const userId = ctx.from?.id;

    if (!userId) {
      await next();

      return;
    }

    botMenu.setContext(ctx);
    botMenu.setUser(userId);

    ctx.menu = botMenu;

    await next();
  };
