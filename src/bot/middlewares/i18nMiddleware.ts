import { createI18n } from '../../i18n';
import { Context, Middleware } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';

export const i18nMiddleware: Middleware<Context<Update>> = async (ctx, next) => {
  ctx.t = createI18n();

  const locale = ctx.from?.language_code;

  if (locale) {
    ctx.t.setLocale(locale);
  }

  next();
};
