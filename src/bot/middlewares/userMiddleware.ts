import { Context, Middleware } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';
import { prisma } from '../../prisma';

export const userMiddleware: Middleware<Context<Update>> = async (ctx, next) => {
  const userId = ctx.from?.id;

  if (!userId) {
    next();

    return;
  }

  const user = await prisma.user.findUnique({
    where: {
      telegramId: userId,
    },
  });

  if (user) {
    ctx.user = user;
  } else {
    const name = ctx.from.first_name;
    const surname = ctx.from.last_name;

    const user = await prisma.user.create({
      data: {
        telegramId: userId,
        name,
        surname,
      },
    });

    ctx.user = user;
  }

  next();
};
