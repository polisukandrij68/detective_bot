import { Context, Middleware, Telegraf } from 'telegraf';
import { PromptOptions, PromptStack, PromptStackNode } from './PromptStack';
import { Update } from 'telegraf/typings/core/types/typegram';
import { SkipPromptButton } from '../../markup/SkipPromptButton';

export class PromptMiddleware {
  private readonly promptStack: PromptStack = new PromptStack();
  private _ctx: Context<Update> | undefined;
  private get ctx() {
    if (!this._ctx) {
      throw new Error('Context is not defined');
    }

    return this._ctx;
  }
  private get tFunction() {
    return this.ctx.t;
  }
  private get userId() {
    return this.ctx.from?.id;
  }
  private get messageId() {
    return this.ctx.message?.message_id;
  }
  private get messageText() {
    // @ts-ignore
    const messageText: string | undefined = this.ctx.message?.text;

    return messageText || '';
  }

  constructor(private readonly bot: Telegraf) {
    this.subscribe();
  }

  public readonly handler: Middleware<Context<Update>> = async (ctx, next) => {
    this._ctx = ctx;
    ctx.prompt = this.prompt.bind(this);

    if (this.hasWaitingCallbacks() && this.messageText) {
      await this.callFirstWaitingCallback();

      return;
    }

    await next();
  };

  private async prompt(question: string, options?: PromptOptions): Promise<string> {
    const replyMessage = await this.ctx.reply(
      question,
      options?.optional ? SkipPromptButton(this.tFunction) : undefined
    );

    const userId = this.userId;

    if (!userId) {
      throw new Error('userId is undefined in telegram context');
    }

    return new Promise<string>((resolve) => {
      this.promptStack.push(
        {
          question,
          callback: resolve,
          messagesIds: [replyMessage.message_id],
          options,
        },
        userId
      );
    });
  }

  private subscribe() {
    this.bot.action('prompt.skip', async (ctx) => {
      this._ctx = ctx;
      await this.skipFirstWaitingCallback();
    });
  }

  private hasWaitingCallbacks() {
    if (!this.userId) {
      return false;
    }

    return Boolean(this.promptStack.getNodesCount(this.userId));
  }

  private async callFirstWaitingCallback() {
    if (!this.userId) {
      return;
    }

    const lastNode = this.promptStack.pop(this.userId);

    if (!lastNode) {
      return;
    }

    try {
      await this.validateAndNotifyOnError(lastNode);
    } catch {
      return;
    }

    lastNode.callback(this.messageText);

    await this.clearUnnecessaryMessages(lastNode);
  }
  private async skipFirstWaitingCallback() {
    if (!this.userId) {
      return;
    }

    const lastNode = this.promptStack.pop(this.userId);

    if (!lastNode) {
      return;
    }

    if (!lastNode.options?.optional) {
      this.promptStack.push(lastNode, this.userId);

      return;
    }

    lastNode.callback('');
    await this.clearUnnecessaryMessages(lastNode);
  }

  private validate(node: PromptStackNode) {
    const validateFunction = node.options?.validate;

    if (!validateFunction) {
      return;
    }

    const error = validateFunction(this.messageText);

    if (error) {
      throw new Error(error);
    }
  }

  private pushAndAppendMessageId(node: PromptStackNode, messageId: number) {
    if (!this.userId) {
      return;
    }

    this.promptStack.push(
      {
        ...node,
        messagesIds: [...node.messagesIds, messageId],
      },
      this.userId
    );
  }

  private async validateAndNotifyOnError(node: PromptStackNode) {
    try {
      this.validate(node);
    } catch (error) {
      if (error instanceof Error) {
        const { message_id: messageId } = await this.ctx.reply(error.message);
        this.pushAndAppendMessageId(node, messageId);
      }

      throw error;
    }
  }

  private async clearUnnecessaryMessages(node: PromptStackNode) {
    if (!node.options?.clearAfterAnswer) {
      return;
    }

    const ids = node.messagesIds;

    if (this.messageId) {
      ids.push(this.messageId);
    }

    for (const id of ids) {
      await this.ctx.deleteMessage(id);
    }
  }
}
