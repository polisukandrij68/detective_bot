import { I18n } from 'i18n';

export const numericValidate = (t: I18n) => (message: string) => {
  const parsedMessage = +message;

  if (Number.isNaN(parsedMessage)) {
    return t.__('errors.numberValidation');
  }
};
