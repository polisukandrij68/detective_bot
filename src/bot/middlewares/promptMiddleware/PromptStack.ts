export type PromptOptions = Partial<{
  clearAfterAnswer: boolean;
  validate: (messageText: string) => string | undefined;
  optional: boolean;
}>;

export type PromptStackNode = {
  question: string;
  callback: (result: string) => void;
  messagesIds: number[];
  options?: PromptOptions;
};

export class PromptStack {
  private stack: Partial<{ [userId: number]: PromptStackNode[] }> = {};

  push(node: PromptStackNode, userId: number) {
    if (!this.stack[userId]) {
      this.stack[userId] = [node];

      return;
    }

    this.stack[userId]?.push(node);
  }

  pop(userId: number): PromptStackNode | undefined {
    return this.stack[userId]?.pop();
  }

  getNodesCount(userId: number): number {
    return this.stack[userId]?.length || 0;
  }
}
